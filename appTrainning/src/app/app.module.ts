import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { CustomInputFieldComponent } from './component/customField/custom-input-field/custom-input-field.component';
import { InputBankCodeComponent } from './component/input/input-bank-code/input-bank-code.component';
import { InputAdressComponent } from './component/input-adress/input-adress.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomInputFieldComponent,
    InputBankCodeComponent,
    InputAdressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
