import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import places from 'places.js'

@Component({
  selector: 'app-input-adress',
  templateUrl: './input-adress.component.html',
  styleUrls: ['./input-adress.component.scss']
})
export class InputAdressComponent implements OnInit, AfterViewInit {

  private autocomplete: any;

  public innerValue = {
    address: '',
    lat: 0.00,
    lng: 0.00
  };

  @ViewChild('ref', { static: true }) public el!: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.autocomplete = places({
      appId: 'plGP23LUP7X0',
      apiKey: '44d36fbd3c0ad30c03de9573d5cd8695',
      container: this.el.nativeElement
    });

    // this.autocomplete.on('change', e => {
    //   if (e.suggestion) {
    //     this.innerValue.lat = e.suggestion.latlng.lat;
    //     this.innerValue.lng = e.suggestion.latlng.lng;
    //     this.innerValue.address = e.suggestion.value;
    //   }
    // });
  }


}
