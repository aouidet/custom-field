import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputBankCodeComponent } from './input-bank-code.component';

describe('InputBankCodeComponent', () => {
  let component: InputBankCodeComponent;
  let fixture: ComponentFixture<InputBankCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputBankCodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputBankCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
