import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import Utils from 'src/app/utils/utils';


@Component({
  selector: 'app-input-bank-code',
  templateUrl: './input-bank-code.component.html',
  styleUrls: ['./input-bank-code.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef( ()=> InputBankCodeComponent)
    }
  ]
})
export class InputBankCodeComponent implements OnInit, ControlValueAccessor {

  public arr = Utils.range(0, 10);

  public innerValue: string = '';

  constructor() { }

  private onChange:  any = () => { };

  private onTouched: any = () => { };

  public isDisabled = false;

  /**
   * call from formControl
   * this method sets the value programmatically
   */
  writeValue(value: any): void {
    this.innerValue = value;
  }

  /**
   * 
   * upon UI element value changes, this method gets triggered
   * indicate which value will be returned to formControl 
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;

  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {
    this.isDisabled = disabled;
  }

  ngOnInit(): void {

    this.arr = Utils.shuffle(this.arr)
    console.log(this.arr);
  }


  /**
   * 
   * @param i update value entred 
   */
  public update(i: number) {
    this.innerValue += i.toString();
    this.onChange(this.innerValue);
  }

  /**
   * reset adata value entred code
   */
  public reset() {
    this.innerValue = '';
    this.onChange(this.innerValue);
  }

}
