import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-custom-input-field',
  templateUrl: './custom-input-field.component.html',
  styleUrls: ['./custom-input-field.component.scss']
})
export class CustomInputFieldComponent implements OnInit {

  public form!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      name: [''],
      code: ['']
    });

    // mettre le form dans un observable
   this.form.valueChanges.subscribe(res => console.log(res));
  
  }

}
