import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CustomInputFieldComponent } from './component/customField/custom-input-field/custom-input-field.component';

const routes: Routes = [

  {
    path: 'customField', component: CustomInputFieldComponent
  },
  {
    path: '', redirectTo: 'customField', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
