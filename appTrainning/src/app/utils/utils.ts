export default class Utils {


    static range = (start: number, end: number): number[] => {
        const inc = (end - start) / Math.abs(end - start);

        return Array.from(
            Array(Math.abs(end - start) + 1),
            (_, i) => start + i * inc
        );
    };

    // methode to shuffle array values*
    static shuffle(arr: number[]): number[] {
        return arr.sort(() =>
            Math.random() - 0.5
        );
    }
}